terraform {
  required_providers {
    amixr = {
      source = "alertmixer/amixr"
      version = "0.2.2"
    }
  }
}

variable "route_id" {
  type = string
}